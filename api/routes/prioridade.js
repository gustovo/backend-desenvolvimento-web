const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.prioridade;
  app
    .route("/prioridade")
    .get(verifyToken, controller.getPrioridade)
    .post(verifyToken, controller.createPrioridade)
    .put(verifyToken, controller.putPrioridade)
    .delete(verifyToken, controller.deletePrioridade);
};