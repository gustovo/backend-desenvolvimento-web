const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.projeto;
  app
    .route("/projeto")
    .get(verifyToken, controller.getProjeto)
    .post(verifyToken, controller.createProjeto)
    .put(verifyToken, controller.putProjeto)
    .delete(verifyToken, controller.deleteProjeto);
};