const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.usuario;
  app
    .route("/usuario")
    .get(verifyToken, controller.getUsuario)
    .post(verifyToken, controller.createUsuario)
    .put(verifyToken, controller.putUsuario)
    .delete(verifyToken, controller.deleteUsuario);
};