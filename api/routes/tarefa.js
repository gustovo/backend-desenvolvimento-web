const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.tarefa;
  app
    .route("/tarefa")
    .get(verifyToken, controller.getTarefa)
    .post(verifyToken, controller.createTarefa)
    .put(verifyToken, controller.putTarefa)
    .delete(verifyToken, controller.deleteTarefa);
};