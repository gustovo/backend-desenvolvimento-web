const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.tarefa_tipo;
  app
    .route("/tarefa_tipo")
    .get(verifyToken, controller.getTarefa_tipo)
    .post(verifyToken, controller.createTarefa_tipo)
    .put(verifyToken, controller.putTarefa_tipo)
    .delete(verifyToken, controller.deleteTarefa_tipo);
};