const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.comentario;
  app
    .route("/comentario")
    .get(verifyToken, controller.getComentario)
    .post(verifyToken, controller.createComentario)
    .put(verifyToken, controller.putComentario)
    .delete(verifyToken, controller.deleteComentario);
};