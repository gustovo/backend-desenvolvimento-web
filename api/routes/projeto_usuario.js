const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.projeto_usuario;
  app
    .route("/projeto_usuario")
    .get(verifyToken, controller.getProjeto_usuario)
    .post(verifyToken, controller.createProjeto_usuario)
    .put(verifyToken, controller.putProjeto_usuario)
    .delete(verifyToken, controller.deleteProjeto_usuario);
};