const { restart } = require("nodemon");

module.exports = app => {

    const controller = {};
    controller.createProjeto_usuario = function (req, res, next) {
        const id_projeto = req.body.id_projeto;
        const id_usuario = req.body.id_usuario;

        app.db.none(`INSERT INTO public.projeto_usuario (id_projeto, id_usuario) VALUES (${id_projeto}, ${id_usuario})`).then(data => {
            res.status(200).json("Ligação criada com sucesso")
        }).catch(function (err) {
            return next(err);
        });

    }

    controller.putProjeto_usuario = function (req, res, next) {
        const id_usuario = req.body.id_usuario;
        const id_projeto = req.body.id_projeto;
        const id = req.query.id;

        app.db.none(`UPDATE public.projeto_usuario SET id_projeto = ${id_projeto}, id_usuario = ${id_usuario} WHERE id = ${id}`).then(data => {
            res.status(200).json("Ligação editada com sucesso")
        }).catch(function (err) {
            return next(err);
        });
    }

    controller.deleteProjeto_usuario = function (req, res, next) {
        const id = req.query.id;

        if (id) {
            app.db.any(`DELETE from public.projeto_usuario WHERE id = ${id}`).then(data => {
                res.status(200).json("Ligação removido com sucesso.");
            }).catch(function (err) {
                return next(err);
            });
        }
        else {
            return res.status(500).json("Id necessário.")
        }
    }

    controller.getProjeto_usuario = function (req, res, next) {
        const id = req.query.id;
        const id_projeto = req.query.id_projeto;
        const id_usuario = req.query.id_usuario;

        if (id) {
            app.db.any(`SELECT pu.id as pu_id,
                            pu.id_usuario as u_id,
                            pu.id_projeto as p_id,
                            p.titulo as titulo,
                            u.nome as nome
                        from public.projeto_usuario pu
                            left join public.projeto p
                                on pu.id_projeto = p.id
                            left join public.usuario u
                                on pu.id_usuario = u.id_usuario WHERE pu.id = ${id}`).then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });
        } else if (id_projeto) {
            app.db.any(`SELECT pu.id as pu_id,
                            pu.id_usuario as u_id,
                            pu.id_projeto as p_id,
                            p.titulo as titulo,
                            u.nome as nome
                        from public.projeto_usuario pu
                            left join public.projeto p
                                on pu.id_projeto = p.id
                            left join public.usuario u
                                on pu.id_usuario = u.id_usuario WHERE id_projeto = ${id_projeto}`).then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });
        } else if (id_usuario) {
            app.db.any(`SELECT pu.id as pu_id,
                            pu.id_usuario as u_id,
                            pu.id_projeto as p_id,
                            p.titulo as titulo,
                            u.nome as nome
                        from public.projeto_usuario pu
                            left join public.projeto p
                                on pu.id_projeto = p.id
                            left join public.usuario u
                                on pu.id_usuario = u.id_usuario WHERE pu.id_usuario = ${id_usuario}`).then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });
        }else {
            app.db.any(`SELECT pu.id as pu_id,
                            pu.id_usuario as u_id,
                            pu.id_projeto as p_id,
                            p.titulo as titulo,
                            u.nome as nome
                        from public.projeto_usuario pu
                            left join public.projeto p
                                on pu.id_projeto = p.id
                            left join public.usuario u
                                on pu.id_usuario = u.id_usuario;`).then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });

        }
    }

    return controller;
}