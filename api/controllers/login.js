const jwt = require("jsonwebtoken");

module.exports = (app) => {
  const controller = {};
  controller.login = function (req, res, next) {
    const SECRET = "test_api";
    const user = req.query.user;
    console.log(user);
    app.db
      .any(`SELECT * from public.usuario where nome LIKE '${user}'`)
      .then((data) => {
        let r = data;
        if (data.length > 0) {
          let valid = r[0];
          const token = jwt.sign({ userId: valid.id }, SECRET, {
            expiresIn: 3000,
          });

          return res
            .status(200)
            .json({ auth: true, token: token, user: r[0].id_usuario });
        }

        res.status(401).end();
      })
      .catch(function (err) {
        return next(err);
      });
  };

  return controller;
};
