const { restart } = require("nodemon");

module.exports = (app) => {
	const controller = {};
	controller.createProjeto = function (req, res, next) {
		const titulo = req.body.titulo;
		const desc = req.body.descricao;
		const data_inicio = req.body.data_inicio || "06/09/2021 00:00:00";
		const data_fim = req.body.data_fim || "06/09/2021 00:00:00";
		const id_criador = req.body.id_criador;
		const id_sistema = req.body.id_sistema;

		app.db
			.any(
				`INSERT INTO public.projeto (titulo, descricao, data_inicio, data_fim, id_criador, created_at, id_sistema) VALUES ('${titulo}', '${desc}', '${data_inicio}', '${data_fim}', ${id_criador}, CURRENT_TIMESTAMP, ${id_sistema});`
			)
			.then((data) => {
				res.status(200).json("Projeto criado com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.putProjeto = function (req, res, next) {
		const titulo = req.body.titulo;
		const descricao = req.body.descricao;
		const data_inicio = req.body.data_inicio || "06/09/2021 00:00:00";
		const data_fim = req.body.data_fim || "06/09/2021 00:00:00";
		const id_criador = req.body.id_criador;
		const id_sistema = req.body.id_sistema;
		const id = req.query.id;

		app.db
			.any(
				`UPDATE public.projeto SET titulo = '${titulo}', descricao = '${descricao}', data_inicio = '${data_inicio}', data_fim = '${data_fim}', id_criador = ${id_criador}, updated_at = CURRENT_TIMESTAMP, id_sistema = ${id_sistema} WHERE id = ${id}`
			)
			.then((data) => {
				res.status(200).json("Projeto editado com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.deleteProjeto = function (req, res, next) {
		const id = req.query.id;

		if (id) {
			app.db
				.any(`DELETE from public.projeto WHERE id = ${id}`)
				.then((data) => {
					res.status(200).json("Projeto removido com sucesso.");
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			return res.status(500).json("Id necessário.");
		}
	};

	controller.getProjeto = function (req, res, next) {
		const id = req.query.id;

		if (id) {
			app.db
				.any(
					`select
                            pro.id as id,
                            pro.titulo as projeto,
                            pro.descricao as descricao,
                            to_char(pro.data_inicio, 'DD/MM/YYYY') as inicio,
                            to_char(pro.data_fim, 'DD/MM/YYYY') as fim,
                            to_char(pro.created_at, 'DD/MM/YYYY') as criacao,
                            to_char(pro.updated_at, 'DD/MM/YYYY') as ultima_edicao,
                            usu.nome as usuario,
                            sis.id_sistema as id_sistema
                        from public.projeto pro
                        left join public.usuario usu
                            on pro.id_criador = usu.id_usuario
                        left join public.sistema sis
                            on pro.id_sistema = sis.id_sistema where pro.id = ${id};`
				)
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			app.db
				.any(
					`select
                                pro.id as id,
                                pro.titulo as projeto,
                                pro.descricao as descricao,
                                to_char(pro.data_inicio, 'DD/MM/YYYY') as inicio,
                                to_char(pro.data_fim, 'DD/MM/YYYY') as fim,
                                to_char(pro.created_at, 'DD/MM/YYYY') as criacao,
                                to_char(pro.updated_at, 'DD/MM/YYYY') as ultima_edicao,
                                usu.nome as usuario,
                                sis.nome as sistema
                            from public.projeto pro
                            left join public.usuario usu
                                on pro.id_criador = usu.id_usuario
                            left join public.sistema sis
                                on pro.id_sistema = sis.id_sistema;`
				)
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		}
	};

	return controller;
};
