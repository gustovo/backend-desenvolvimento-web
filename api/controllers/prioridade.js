const { restart } = require("nodemon");

module.exports = (app) => {
	const controller = {};
	controller.createPrioridade = function (req, res, next) {
		// console.log(req.body);
		// Object.keys(req.body).forEach(key => {
		//     console.log(req.body[key]);
		//     if(req.body[key] == undefined)
		//         return res.status(500).json(`Campo ${key} vazio!`)
		// })
		const desc = req.body.descricao;

		app.db
			.none(`INSERT INTO public.prioridade (descricao) VALUES ('${desc}')`)
			.then((data) => {
				res.status(200).json("Prioridade criada com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.putPrioridade = function (req, res, next) {
		app.db
			.none(
				`UPDATE public.prioridade SET descricao = '${req.body.descricao}' WHERE id = ${req.query.id}`
			)
			.then((data) => {
				res.status(200).json("Prioridade editada com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.deletePrioridade = function (req, res, next) {
		if (req.query.id) {
			app.db
				.any(`DELETE from public.prioridade WHERE id = ${req.query.id}`)
				.then((data) => {
					res.status(200).json("Prioridade removida com sucesso.");
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			return res.status(500).json("Id necessário.");
		}
	};

	controller.getPrioridade = function (req, res, next) {
		if (req.query.id) {
			app.db
				.any(`SELECT * from public.prioridade WHERE id = ${req.query.id}`)
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			app.db
				.any("SELECT * from public.prioridade")
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		}
	};

	return controller;
};
