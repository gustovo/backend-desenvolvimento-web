const { restart } = require("nodemon");

module.exports = (app) => {
	const controller = {};
	controller.createComentario = function (req, res, next) {
		const id_usuario = req.body.id_usuario;
		const id_tarefa = req.body.id_tarefa;
		const desc = req.body.descricao;
		const id_pai_comentario = req.body.id_pai_comentario || 84;

		app.db
			.none(
				`INSERT INTO public.comentario (id_usuario, id_tarefa, descricao, created_at, id_pai_comentario) VALUES ('${id_usuario}', '${id_tarefa}', '${desc}', CURRENT_TIMESTAMP, '${id_pai_comentario}')`
			)
			.then((data) => {
				res.status(200).json("Comentario criado com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.putComentario = function (req, res, next) {
		const id = req.query.id;
		const id_usuario = req.body.id_criador;
		const id_tarefa = req.body.id_tarefa;
		const desc = req.body.descricao;
		const id_pai_comentario = req.body.id_pai_comentario || 84;
		app.db
			.none(
				`UPDATE public.comentario SET id_usuario = ${id_usuario}, id_tarefa = ${id_tarefa}, descricao = '${desc}', updated_at = CURRENT_TIMESTAMP, id_pai_comentario = ${id_pai_comentario} WHERE id = ${id}`
			)
			.then((data) => {
				res.status(200).json("Comentario editado com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.deleteComentario = function (req, res, next) {
		const id = req.query.id;

		if (id) {
			app.db
				.any(`DELETE from public.comentario WHERE id = ${id}`)
				.then((data) => {
					res.status(200).json("Comentario removido com sucesso.");
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			return res.status(500).json("Id necessário.");
		}
	};

	controller.getComentario = function (req, res, next) {
		const id = req.query.id;

		if (id) {
			app.db
				.any(
					`select
                            com.id as id,
                            com.descricao as descricao,
                            to_char(com.created_at, 'DD/MM/YYYY') as criado,
                            to_char(com.updated_at, 'DD/MM/YYYY') as editado,
                            usu.nome as nome,
                            tar.titulo as tarefa,
                            com2.descricao as comentario
                        from comentario com
                        left join public.usuario usu
                            on com.id_usuario = usu.id_usuario
                        left join public.tarefa tar
                            on com.id_tarefa = tar.id
                        left join public.comentario com2
                            on com.id = com2.id_pai_comentario where com.id = ${id};`
				)
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			app.db
				.any(
					`select
                            com.id as id,
                            com.descricao as descricao,
                            to_char(com.created_at, 'DD/MM/YYYY') as criado,
                            to_char(com.updated_at, 'DD/MM/YYYY') as editado,
                            usu.nome as nome,
                            tar.titulo as tarefa,
                            com2.descricao as comentario
                        from comentario com
                        left join public.usuario usu
                            on com.id_usuario = usu.id_usuario
                        left join public.tarefa tar
                            on com.id_tarefa = tar.id
                        left join public.comentario com2
                            on com.id = com2.id_pai_comentario;`
				)
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		}
	};

	return controller;
};
