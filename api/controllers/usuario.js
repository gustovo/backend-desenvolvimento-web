const { restart } = require("nodemon");

module.exports = app => {

    const controller = {};
    controller.createUsuario = function (req, res, next) {
        // console.log(req.body);
        // Object.keys(req.body).forEach(key => {
        //     console.log(req.body[key]);
        //     if(req.body[key] == undefined)
        //         return res.status(500).json(`Campo ${key} vazio!`)
        // })
        const nome = req.body.nome;

        app.db.none(`INSERT INTO public.usuario (nome) VALUES ('${nome}')`).then(data => {
            res.status(200).json("Usuário criado com sucesso")
        }).catch(function (err) {
            return next(err);
        });

    }

    controller.putUsuario = function (req, res, next) {
        const nome = req.body.nome;
        const id = req.query.id;

        app.db.none(`UPDATE public.usuario SET nome = '${nome}' WHERE id_usuario = ${id}`).then(data => {
            res.status(200).json("Usuário editado com sucesso")
        }).catch(function (err) {
            return next(err);
        });
    }

    controller.deleteUsuario = function (req, res, next) {
        const id = req.query.id;

        if (id) {
            app.db.any(`DELETE from public.usuario WHERE id_usuario = ${id}`).then(data => {
                res.status(200).json("Usuário removido com sucesso.");
            }).catch(function (err) {
                return next(err);
            });
        }
        else {
            return res.status(500).json("Id necessário.")
        }
    }

    controller.getUsuario = function (req, res, next) {
        const id = req.query.id;

        if (id) {
            app.db.any(`SELECT * from public.usuario WHERE id_usuario = ${id}`).then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });
        }
        else {
            app.db.any('SELECT * from public.usuario').then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });

        }
    }

    return controller;
}