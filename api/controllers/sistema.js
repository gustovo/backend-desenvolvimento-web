const { restart } = require("nodemon");

module.exports = app => {

    const controller = {};
    controller.createSistema = function (req, res, next) {
        const nome = req.body.nome;

        app.db.none(`INSERT INTO public.sistema (nome) VALUES ('${nome}')`).then(data => {
            res.status(200).json("Sistema criado com sucesso")
        }).catch(function (err) {
            return next(err);
        });

    }

    controller.putSistema = function (req, res, next) {
        const nome = req.body.nome;
        const id = req.query.id;

        app.db.none(`UPDATE public.sistema SET nome = '${nome}' WHERE id_sistema = ${id}`).then(data => {
            res.status(200).json("Sistema editado com sucesso")
        }).catch(function (err) {
            return next(err);
        });
    }

    controller.deleteSistema = function (req, res, next) {
        const id = req.query.id;

        if (id) {
            app.db.any(`DELETE from public.sistema WHERE id_sistema = ${id}`).then(data => {
                res.status(200).json("Sistema removido com sucesso.");
            }).catch(function (err) {
                return next(err);
            });
        }
        else {
            return res.status(500).json("Id necessário.")
        }
    }

    controller.getSistema = function (req, res, next) {
        const id = req.query.id;

        if (id) {
            app.db.any(`SELECT * from public.sistema WHERE id_sistema = ${id}`).then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });
        }
        else {
            app.db.any('SELECT * from public.sistema').then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });

        }
    }

    return controller;
}