const { restart } = require("nodemon");

module.exports = app => {

    const controller = {};
    controller.createTarefa_tipo = function (req, res, next) {
        const desc = req.body.descricao;

        app.db.none(`INSERT INTO public.tarefa_tipo (descricao) VALUES ('${desc}')`).then(data => {
            res.status(200).json("Tipo de tarefa criada com sucesso")
        }).catch(function (err) {
            return next(err);
        });

    }

    controller.putTarefa_tipo = function (req, res, next) {
        const desc = req.body.descricao;
        const id = req.query.id;
        app.db.none(`UPDATE public.tarefa_tipo SET descricao = '${desc}' WHERE id = ${id}`).then(data => {
            res.status(200).json("Tipo de tarefa editada com sucesso")
        }).catch(function (err) {
            return next(err);
        });
    }

    controller.deleteTarefa_tipo = function (req, res, next) {
        const id = req.query.id;

        if (id) {
            app.db.any(`DELETE from public.tarefa_tipo WHERE id = ${id}`).then(data => {
                res.status(200).json("Tipo de tarefa removida com sucesso.");
            }).catch(function (err) {
                return next(err);
            });
        }
        else {
            return res.status(500).json("Id necessário.")
        }
    }

    controller.getTarefa_tipo = function (req, res, next) {
        const id = req.query.id;

        if (id) {
            app.db.any(`SELECT * from public.tarefa_tipo WHERE id = ${id}`).then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });
        }
        else {
            app.db.any('SELECT * from public.tarefa_tipo').then(data => {
                res.status(200).json(data);
            }).catch(function (err) {
                return next(err);
            });

        }
    }

    return controller;
}