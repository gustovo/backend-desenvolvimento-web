const { restart } = require("nodemon");

module.exports = (app) => {
  const controller = {};
  controller.createGrupo = function (req, res, next) {
    const desc = req.body.descricao;
    const id_projeto = req.body.id_projeto;

    app.db
      .none(
        `INSERT INTO public.grupo (id_projeto, descricao) VALUES (${id_projeto}, '${desc}')`
      )
      .then((data) => {
        res.status(200).json("Grupo criado com sucesso");
      })
      .catch(function (err) {
        return next(err);
      });
  };

  controller.putGrupo = function (req, res, next) {
    const desc = req.body.descricao;
    const id_projeto = req.body.id_projeto;
    const id = req.query.id;

    app.db
      .none(
        `UPDATE public.grupo SET id_projeto = ${id_projeto}, descricao = '${desc}' WHERE id = ${id}`
      )
      .then((data) => {
        res.status(200).json("Grupo editado com sucesso");
      })
      .catch(function (err) {
        return next(err);
      });
  };

  controller.deleteGrupo = function (req, res, next) {
    const id = req.query.id;

    if (id) {
      app.db
        .any(`DELETE from public.grupo WHERE id = ${id}`)
        .then((data) => {
          res.status(200).json("Grupo removido com sucesso.");
        })
        .catch(function (err) {
          return next(err);
        });
    } else {
      return res.status(500).json("Id necessário.");
    }
  };

  controller.getGrupo = function (req, res, next) {
    const id = req.query.id;
    console.log(req);

    if (id) {
      app.db
        .any(`SELECT * from public.grupo WHERE id = ${id}`)
        .then((data) => {
          res.status(200).json(data);
        })
        .catch(function (err) {
          return next(err);
        });
    } else {
      app.db
        .any("SELECT * from public.grupo")
        .then((data) => {
          res.status(200).json(data);
        })
        .catch(function (err) {
          return next(err);
        });
    }
  };

  return controller;
};
