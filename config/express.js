const express = require("express");
const config = require("config");
const cors = require("cors");
const consign = require("consign");
const pgp = require("pg-promise")({});

module.exports = () => {
  const app = express();
  cn = {
    host: "devweb.chiqahlpkytu.sa-east-1.rds.amazonaws.com",
    port: 5432,
    database: "postgres",
    user: "root",
    password: "123456789",
  };
  const db = pgp(cn);
  app.set("port", process.env.PORT || config.get("server.port"));
  app.db = db;
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());
  app.use(cors());
  app.options("*", cors());
  // require('../api/routes/envio')(app);

  consign({ cwd: "api" })
    .then("data")
    .then("controllers")
    .then("routes")
    .into(app);

  return app;
};
